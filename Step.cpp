//
// Created by Chocobon on 13/04/2021.
//

#include "Step.h"

Step::Step(sf::Vector2i center, float radius) : center(center), radius(radius), time(0) {

}

Step::Step(sf::Vector2i center, float radius, float time) : center(center), radius(radius), time(time) {

}
