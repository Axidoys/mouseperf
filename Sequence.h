//
// Created by Chocobon on 13/04/2021.
//

#ifndef MOUSEPERF_SEQUENCE_H
#define MOUSEPERF_SEQUENCE_H

#include <string>
#include <SFML/Graphics/Drawable.hpp>
#include "Step.h"

#define STATE_WAIT -1
#define STATE_END -2

class Sequence : public sf::Drawable {
private:

    int stage = STATE_WAIT;

    std::vector<Step> steps;

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

public:
    Sequence(); //new empty sequence
    void loadStr(std::string);

    //Sequence(std::string seq);
    bool passStep(); //each frame
    bool passStep(sf::Vector2i mousePosition); //each frame

    int getStepNumber() { return stage; }


    static bool pointInCircle(sf::Vector2i center, float radius, sf::Vector2i point);
};


#endif //MOUSEPERF_SEQUENCE_H
