//
// Created by Chocobon on 13/04/2021.
//

#ifndef MOUSEPERF_PERFTEST_H
#define MOUSEPERF_PERFTEST_H

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "Sequence.h"

using namespace sf;

//A test is instanciated with dynamic var (screen size etc.)
//A sequence is defined statically, from a file or a string

class PerfTest
{
private:
    RenderWindow window;
    Texture texture;
    Sprite sprite;
    Font font;
    Text text;
    Music music;

    Sequence seq;

    Clock clk;

public:
    PerfTest(int width, int height, std::string title);
    bool loadRes();
    bool loadSeq(std::string);
    void loop();
};

#endif //MOUSEPERF_PERFTEST_H
