cmake_minimum_required(VERSION 3.15)
project(mousePerf)

set(CMAKE_CXX_STANDARD 17)

set(SFML_DIR "C:\\Program Files\\zBuild\\SFML-2.5.1\\lib\\cmake\\SFML\\")
find_package(SFML 2.5.1 COMPONENTS graphics audio REQUIRED)
include_directories(C:\\Program Files\\zBuild\\SFML-2.5.1\\include)



add_executable(mousePerf main.cpp PerfTest.cpp PerfTest.h Sequence.cpp Sequence.h Step.cpp Step.h)


target_link_libraries(mousePerf sfml-graphics sfml-audio)