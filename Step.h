//
// Created by Chocobon on 13/04/2021.
//

#ifndef MOUSEPERF_STEP_H
#define MOUSEPERF_STEP_H

//don't use polymorphism at this moment
//only discs

#include <SFML/System/Vector2.hpp>
#include <SFML/System/Clock.hpp>


class Step {
private:

public:
    sf::Vector2i center;
    float radius;
    Step(sf::Vector2i center, float radius);
    Step(sf::Vector2i center, float radius, float time);
    float time;
    sf::Clock clk;
    bool begin = false;
};


#endif //MOUSEPERF_STEP_H
