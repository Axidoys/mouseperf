//
// Created by Chocobon on 13/04/2021.
//
#include <string>
#include <iostream>
#include "PerfTest.h"

PerfTest::PerfTest(int width, int height, std::string title) : window(VideoMode(width, height), title), seq() {

}

bool PerfTest::loadRes(){
    bool ret = true;

    if (!texture.loadFromFile("cute_image2.jpg")) {
        ret = false;
    }
    sprite = Sprite(texture);

    if (!font.loadFromFile("arial.ttf")) {
        ret = false;
    }
    text = Text("Hello SFML", font, 50);

    if (!music.openFromFile("nice_music.ogg")){
        ret = false;
    }

    return ret;
}

bool PerfTest::loadSeq(std::string){
    //1600, 900 => milieu 800,450
    seq.loadStr("(800;450)/15/3|(100;800)/50/1|(100;100)/50/1|(1500;100)/50/1|(1500;800)/50/1|(100;800)/50/0.5|(1500;800)/50/0.5|(100;800)/40/0.5|(300;100)/30/1|(500;100)/25/0.4|(300;150)/25/0.4|(500;200)/20/0.4|(300;250)/15/0.4|(500;300)/15/0.4|(600;600)/60/1|(200;600)/50/0.3|(600;600)/50/0.3|(200;600)/50/0.3|(600;600)/50/1");
    return true;
}

void PerfTest::loop(){
    //music.play();

    // Start the game loop
    while (window.isOpen())
    {
        // Process events
        Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            if (event.type == Event::Closed)
                window.close();
            else if (event.type == Event::KeyReleased){
                if(event.key.code == Keyboard::Space){
                    seq.passStep();
                    clk.restart();
                }
            }
        }



        //"physic"
        if(seq.getStepNumber()!=STATE_WAIT){
            seq.passStep(Mouse::getPosition(window));
        }
        if(seq.getStepNumber()==STATE_END){
            std::cout << "Finished ! " << clk.getElapsedTime().asSeconds() << std::endl ;
            seq.passStep();
        }

        // Clear screen
        window.clear(Color(245, 245, 245, 255));

        // Draw the sprite
        window.draw(sprite);
        // Draw the string
        window.draw(text);

        // Draw the overlay
        std::string debugStr;
        if(seq.getStepNumber()==STATE_WAIT)
            debugStr = "DEBUG MODE";
        else {
            char buff[100];
            sprintf(buff, "s(%d) t(%.1f)", seq.getStepNumber(), clk.getElapsedTime().asSeconds());
            debugStr = buff;
        }
        Text textDebug = Text(debugStr, font, 30);
        textDebug.setPosition(0, 50);
        window.draw(textDebug);

        window.draw(seq);

        // Update the window
        window.display();
    }
}