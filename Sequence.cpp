//
// Created by Chocobon on 13/04/2021.
//

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <iostream>
#include "Sequence.h"

Sequence::Sequence() : stage(STATE_WAIT), steps() {

/*    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});
    steps.push_back({{200, 300}, 20.f});
    steps.push_back({{600, 300}, 20.f});*/
}

Step* strToStep(std::string str){
    sf::Vector2i center;
    float radius;
    float time;

    if(sscanf(str.c_str(), "(%d;%d)/%f/%f", &center.x, &center.y, &radius, &time) ==4)
        return new Step(center, radius, time);

    return nullptr;
}

void Sequence::loadStr(std::string str) {
    if(str.length() == 0)
        return ;

    std::string delimiter = "|";

    size_t pos = 0;
    std::string token;
    while ((pos = str.find(delimiter)) != std::string::npos) {
        token = str.substr(0, pos);
        Step* s = strToStep(token);
        if(s!= nullptr)
            steps.push_back(*s);

        str.erase(0, pos + delimiter.length());
    }
    Step* s = strToStep(str);
    if(s!= nullptr)
        steps.push_back(*s);
    else
    std::cout << s << std::endl;
}

bool Sequence::passStep() {
    if(stage==STATE_WAIT){
        stage=0;
        return true;
    }
    else if(stage==STATE_END){
        stage=STATE_WAIT;
        return true;
    }
    return false;
}




void Sequence::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    if(stage==STATE_WAIT)
        return;

    sf::CircleShape shape(steps[stage].radius);
    shape.setFillColor({0, 0, 0});
    if(steps[stage].begin) {
        shape.setOutlineColor(sf::Color(255, 80, 80));
        shape.setOutlineThickness(3.f);
    }
    shape.setPosition((sf::Vector2f)steps[stage].center - steps[stage].radius*sf::Vector2f(1.f,1.f));
    target.draw(shape, states);
}

bool Sequence::passStep(sf::Vector2i mousePosition) {
    if(pointInCircle(steps[stage].center, steps[stage].radius, mousePosition)){
        if(!steps[stage].begin){
            steps[stage].clk.restart();
            steps[stage].begin = true;
        }

        if(steps[stage].clk.getElapsedTime().asSeconds() > steps[stage].time) //success !!
        {
            stage += 1;
            if(stage>steps.size()-1)
                stage = STATE_END;
            return true;
        }
    }
    else{
        steps[stage].begin = false;
    }
    return false;
}

bool Sequence::pointInCircle(sf::Vector2i center, float radius, sf::Vector2i point) {
    return (point.x - center.x) * (point.x - center.x) +
           (point.y - center.y) * (point.y - center.y) <= radius * radius;
}


/*
void Sequence::draw(sf::RenderTarget target, sf::RenderStates states) {

}*/
